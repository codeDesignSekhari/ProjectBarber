# Change Log

## [Unreleased]

- __Added__ : `General` CHANGELOG.md : Création d'un nouveau fichier pour garder une trace des changements
- __Added__ : `Page Appointement` Ajout de la page d'un calendrier pour l'ajout des séances avec la librairie Fullcalendar.
- __Added__ : `Page Message` Ajout de la page Messages.
- __Added__ : `Page Prestation` Ajout de la page des prestations, pour visualiser les séance consommé.
- __Added__ : `Page profil` Ajout de la page profil, avec visualisation de ses informations personnelles.

- __Added__ : `Page -Back Admin- profil User` Ajout de la page profil user, pour voir la fiche client selectionné.
- __Added__ : `Page -Back Admin- setting` Ajout de la page paramétre.
- __Added__ : `Page -Back Admin- notification` Ajout de la page notification.
- __Added__ : `Page -Back Admin- gestion utilisateurs` Ajout de la page gestion utilisateurs.
- __Added__ : `Page -Back Admin- gestion team` Ajout de la page gestion team, visualisation des employés pour l'administrateur.
- __Added__ : `Page -Back Admin- gestion utilisateurs` Ajout de la page gestion utilisateurs.

- __Added__ : `Fixture` Ajout fixture initialization des données.

- __Added__ : `ReadMe` Ajout du fichier et du contenu pour initialisation du projet barber README.md
- __Added__ : `Service sendmail` Ajout du fichier et contenu pour envoie de Email.
- __Added__ : `twig Email` Ajout des fichiers mail et creation interface css/html.  
