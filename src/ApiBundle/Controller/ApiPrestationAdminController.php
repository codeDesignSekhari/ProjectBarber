<?php
namespace ApiBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Delete;

use AppBundle\Entity\ServiceCatalog;
use AppBundle\Entity\User;



class ApiPrestationAdminController extends FosRestController
{

    /**
     * @Rest\Post("/prestations/new")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @return Response
     */
    public function postPrestationAction(Request $request)
    {
        try {
            if (!$this->getUser()->hasRole('ROLE_ADMIN')) return new JsonResponse('Permission invalide', Response::HTTP_FORBIDDEN);
            $title = $request->get('titleCredit');
            $price = $request->get('priceCredit');
            $credit = $request->get('credit');
            // $this->get('logger')->info('000000000000000000000000000000000000', [$title]);
            $currentAdmin = $this->getUser();
            $this->get('app.manage_Prestation')->createPrestation($credit, $price, $title, $currentAdmin, true);
            return new JsonResponse("L'offre à bien été ajouté", Response::HTTP_OK);
        } catch (\Exception $e) {
            return new JsonResponse([$e->getMessage()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * @Get("/prestations")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @return Response
     */
    public function getPrestationAction()
    {
        try {
            if (!$this->getUser()->hasRole('ROLE_ADMIN')) return new JsonResponse('Permission invalide', Response::HTTP_FORBIDDEN);
            $admin = $this->getUser();
            $serviceCatalog = $this->getDoctrine()->getRepository(ServiceCatalog::class)->findByAffectedAdmin($admin);
            $view = View::create($serviceCatalog);
            $view->setFormat('json');
            return $view;
        } catch (\Exception $e) {
            return new JsonResponse([$e->getMessage()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * @Delete("prestations/delete/{id}")
     * @Security("has_role('ROLE_ADMIN')")
     * @param Request $request
     * @return Response
     */
    public function deletePrestationAction($id)
    {
        try {
            $serviceCatalog = $this->getDoctrine()->getRepository(ServiceCatalog::class)->findOneById($id);
            if (null === $serviceCatalog) return new JsonResponse("La prestation n'existe pas ou plus", Response::HTTP_UNPROCESSABLE_ENTITY);
            $em = $this->getDoctrine()->getManager();
            $em->remove($serviceCatalog);
            $em->flush();
            return new JsonResponse("La prestation à bien été supprimer", Response::HTTP_OK);
        } catch (\Exception $e) {
            return new JsonResponse([$e->getMessage()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

}
