<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\Credit;
use AppBundle\Entity\Card;

use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\View\ViewHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use Symfony\Component\Validator\Constraints\Date;

class ApiStripeController extends FOSRestController
{

  /**
  * @Rest\Post("/stripe/customer/new")
  * @param Request $request
  * @return Response
  */
  public function CreateCustomerAction(Request $request)
  {
    // $apiStripeController = new ApiStripeController($this->getParameter('stripe_public_key'));
    $apikey = \Stripe\Stripe::setApiKey($this->getParameter('stripe_private_key'));
    // if (!$apikey) return new JsonResponse("Stripe ne reconnait pas API Key !, veuillez contacter". $apikey." l'administrateur du site.", Response::HTTP_OK);
    $userCurrent = $this->getUser();
    $idOffer = $request->get('idOffer');
    $idToken = $request->get('stripeToken');
    
    if ($idToken) {
      $SelectedUserCurrentToken = $this->getDoctrine()->getRepository(User::class)->find($userCurrent);
      $tokenstripeExist = $SelectedUserCurrentToken->getTokenStripeUser();

      $currentUserEmail = $userCurrent->getEmail();

      if ($tokenstripeExist === null) $this->createCustomer($currentUserEmail, $idToken);
      $this->createCharge($idToken, $idOffer);
      return $this->redirectToRoute('front_confirmation_purshase');
    } else {
      return new JsonResponse("Un probléme est survenue", Response::HTTP_OK);
    }
  }


  public function createCustomer($currentUserEmail, $idToken)
  {
    try {
      $userCurrent = $this->getUser();
      //create Customer
      $c = \Stripe\Customer::create([
        "email" => $currentUserEmail,
        "source" => $idToken,
      ]);
      $setToken = $userCurrent->setTokenStripeUser($c->id);
      if(!$c) return new View( 'codeError : C30 / Une erreur est survenue, le paiement a été annuler', Response::HTTP_UNPROCESSABLE_ENTITY);
      $em = $this->getDoctrine()->getManager();
      $em->persist($setToken);
      $em->flush();

    } catch (\Exception $e) {
      return new View([$e->getMessage()], Response::HTTP_UNPROCESSABLE_ENTITY);
    }
  }

  public function createCharge($idToken, $idOffer)
  {
    try {
      $offreCredit = $this->getDoctrine()->getRepository(Credit::class)->find($idOffer);
      $price = $offreCredit->getPrice();
      $userCurrent = $this->getUser();
      
      //payment
      $amountCents = $price * 100;
      $payment = \Stripe\Charge::create([
        'source' => $idToken,
        'amount' => $amountCents,
        "currency" => "eur",
        'description' => "Payment"
        ]);
      $m = new \Moment\Moment('now', 'Europe/Berlin');
      if(!$payment) return new View('codeError : pay20 / Une erreur est survenue, le paiement a été annuler', Response::HTTP_UNPROCESSABLE_ENTITY);
      $card = new Card();
      $card->setCardId($payment->source->id)
            ->setDate($m)
            ->setExpYear($payment->source->exp_year)
            ->setExpMonth($payment->source->exp_month)
            ->setLast4($payment->source->last4);
      
      $creditOffer = $offreCredit->getCredit();
      $userCurrent->setSoldeCredit($userCurrent->getSoldeCredit() + $creditOffer);
    
      $em = $this->getDoctrine()->getManager();
      $em->persist($userCurrent);
      $em->persist($card);
      $em->flush();
    } catch (\Exception $e) {
      return new View([$e->getMessage()], Response::HTTP_UNPROCESSABLE_ENTITY);
    }
  }
}
