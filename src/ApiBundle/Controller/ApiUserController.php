<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\Credit;
use AppBundle\Entity\Card;

use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\View\ViewHandler;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use Symfony\Component\Validator\Constraints\Date;

class ApiUserController extends FOSRestController
{

    /**
     * @DELETE("/delete/user/{id}")
     * @Security("has_role('ROLE_FRONT_ACCESS')")
     * @param Request $request
     * @return Response
     */
    public function DeleteUserAction($id)
    {
        if (!$this->getUser()->hasRole('ROLE_FRONT_ACCESS')) return new JsonResponse('Permission invalide', Response::HTTP_FORBIDDEN);
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        if (!$user) return new JsonResponse('error'. $id, Response::HTTP_OK);;
       
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        return new JsonResponse(Response::HTTP_OK);
    }
}
