<?php

/**
 * DefaultController.php
 * @copyright 2018-2019 Barber
 * @author  Youssouf SEKHARI <You.sekhari@gmail.com>
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use  Symfony\Component\Intl\DateFormatter\IntlDateFormatter;
use Nesk\Puphpeteer\Puppeteer;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Validator\Constraints\Date;
use AppBundle\Entity\Invoice;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use AppBundle\Entity\ServiceList;

class InvoiceController extends Controller
{
  /**
   * @Route("/invoice/pdf/{id}", name="invoicePdf")
   * @Security("has_role('ROLE_FRONT_ACCESS' ) ")
   */
  public function InvoicePdfAction($id)
  {
    $user = $this->getUser();

    $invoice = $this->getDoctrine()->getRepository(Invoice::class)->find($id);
    $serviceList = $invoice->getServiceList();

    $ht = $serviceList->getHT();
    $price = $serviceList->getPrice();
    $ttc = (($ht * $price) / 100);
    $total = round($price + $ttc);

    $loader = new \Twig\Loader\FilesystemLoader($this->get('kernel')->getProjectDir() . '/src/AppBundle/Ressources/views/pdf');
    $twig = new \Twig\Environment($loader);
    $content = $twig->render('invoice.html.twig', [
      'user' => $user,
      'invoice' => $invoice,
      'totalPrice' => $total
    ]);

    $contentUrl = 'data:text/html;charset=utf8;base64,' . base64_encode($content);
    $puppeteer = new Puppeteer();
    $browser = $puppeteer->launch([
    'args' => ['--no-sandbox', '--disable-setuid-sandbox'],
    ]);
    $page = $browser->newPage();
    $page->goto($contentUrl);
    $date = new \DateTime('now');
    $page->pdf(['path' => sys_get_temp_dir() . '/invoice.pdf']);
    $browser->close();
    $pdfInvoice = new \SplFileInfo(sys_get_temp_dir() . '/invoice.pdf');
    return new BinaryFileResponse($pdfInvoice);
  }
}
