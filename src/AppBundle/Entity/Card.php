<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\ExclusionPolicy;

/**
 * Card
 *
 * @ORM\Table(name="card")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CardRepository")
 * 
 */
class Card
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cardId", type="string", length=255, nullable=true)
     */
    private $cardId;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="exp_year", type="integer", nullable=true)
     */
    private $exp_year;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="exp_month", type="integer", nullable=true)
     */
    private $exp_month;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="last4", type="integer", nullable=true)
     */
    private $last4;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    public $user;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date.
     *
     * @param \DateTime|null $date
     *
     * @return Card
     */
    public function setDate($date = null)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime|null
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set cardId.
     *
     * @param string|null $cardId
     *
     * @return Card
     */
    public function setCardId($cardId = null)
    {
        $this->cardId = $cardId;

        return $this;
    }

    /**
     * Get cardId.
     *
     * @return string|null
     */
    public function getCardId()
    {
        return $this->cardId;
    }

    /**
     * Set expYear.
     *
     * @param int|null $expYear
     *
     * @return Card
     */
    public function setExpYear($expYear = null)
    {
        $this->exp_year = $expYear;

        return $this;
    }

    /**
     * Get expYear.
     *
     * @return int|null
     */
    public function getExpYear()
    {
        return $this->exp_year;
    }

    /**
     * Set expMonth.
     *
     * @param int|null $expMonth
     *
     * @return Card
     */
    public function setExpMonth($expMonth = null)
    {
        $this->exp_month = $expMonth;

        return $this;
    }

    /**
     * Get expMonth.
     *
     * @return int|null
     */
    public function getExpMonth()
    {
        return $this->exp_month;
    }

    /**
     * Set last4.
     *
     * @param int|null $last4
     *
     * @return Card
     */
    public function setLast4($last4 = null)
    {
        $this->last4 = $last4;

        return $this;
    }

    /**
     * Get last4.
     *
     * @return int|null
     */
    public function getLast4()
    {
        return $this->last4;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->id_user = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add idUser.
     *
     * @param \AppBundle\Entity\User $idUser
     *
     * @return Card
     */
    public function addIdUser(\AppBundle\Entity\User $idUser)
    {
        $this->id_user[] = $idUser;

        return $this;
    }

    /**
     * Remove idUser.
     *
     * @param \AppBundle\Entity\User $idUser
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeIdUser(\AppBundle\Entity\User $idUser)
    {
        return $this->id_user->removeElement($idUser);
    }

    /**
     * Get idUser.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdUser()
    {
        return $this->id_user;
    }

    /**
     * Set user.
     *
     * @param \AppBundle\Entity\User|null $user
     *
     * @return Card
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }
}
