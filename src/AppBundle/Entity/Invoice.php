<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Invoice
 *
 * @ORM\Table(name="invoice")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\InvoiceRepository")
 */
class Invoice
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="ServiceList", inversedBy="invoice", cascade={"persist", "remove"})
     */
    private $serviceList;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return Invoice
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->serviceList = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add serviceList.
     *
     * @param \AppBundle\Entity\ServiceList $serviceList
     *
     * @return Invoice
     */
    public function addServiceList(\AppBundle\Entity\ServiceList $serviceList)
    {
        $this->serviceList[] = $serviceList;

        return $this;
    }

    /**
     * Remove serviceList.
     *
     * @param \AppBundle\Entity\ServiceList $serviceList
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeServiceList(\AppBundle\Entity\ServiceList $serviceList)
    {
        return $this->serviceList->removeElement($serviceList);
    }

    /**
     * Get serviceList.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServiceList()
    {
        return $this->serviceList;
    }

    /**
     * Set serviceList.
     *
     * @param \AppBundle\Entity\ServiceList|null $serviceList
     *
     * @return Invoice
     */
    public function setServiceList(\AppBundle\Entity\ServiceList $serviceList = null)
    {
        $this->serviceList = $serviceList;

        return $this;
    }
}
