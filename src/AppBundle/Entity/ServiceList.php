<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ServiceList
 *
 * @ORM\Table(name="service_list")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ServiceListRepository")
 */
class ServiceList
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    // /**
    //  * @ORM\ManyToOne(targetEntity="User")
    //  */
    // private $affectedAdmin;

    /**
     * @var string
     *
     * @ORM\Column(name="haircut", type="string", length=255)
     */
    private $haircut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_haircut", type="datetime")
     */
      private $dateHaircut;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="integer")
     */
    private $price;

    /**
     * @var int
     *
     * @ORM\Column(name="ht", nullable=true, type="float")
     */
    private $HT;

    /**
     * A Service is affected to a customer (front user).
     * @Assert\NotNull(message = "A service requires a customer affected")
     * @ORM\ManyToOne(targetEntity="User", inversedBy="affectedService", cascade={"persist", "remove"})
     */
    protected $affectedCustomer;

    /**
     * @ORM\OneToMany(targetEntity="Invoice", mappedBy="serviceList", cascade={"persist", "remove"})
     */
    protected $invoice;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set haircut.
     *
     * @param string $haircut
     *
     * @return ServiceList
     */
    public function setHaircut($haircut)
    {
        $this->haircut = $haircut;

        return $this;
    }

    /**
     * Get haircut.
     *
     * @return string
     */
    public function getHaircut()
    {
        return $this->haircut;
    }

    /**
     * Set dateHaircut.
     *
     * @param \DateTime $dateHaircut
     *
     * @return ServiceList
     */
    public function setDateHaircut($dateHaircut)
    {
        $this->dateHaircut = $dateHaircut;

        return $this;
    }

    /**
     * Get dateHaircut.
     *
     * @return \DateTime
     */
    public function getDateHaircut()
    {
        return $this->dateHaircut;
    }

    /**
     * Set price.
     *
     * @param int $price
     *
     * @return ServiceList
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add user.
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return ServiceList
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->user[] = $user;

        return $this;
    }

    /**
     * Remove user.
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        return $this->user->removeElement($user);
    }

    /**
     * Get user.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set affectedCustomer.
     *
     * @param \AppBundle\Entity\User|null $affectedCustomer
     *
     * @return ServiceList
     */
    public function setAffectedCustomer(\AppBundle\Entity\User $affectedCustomer = null)
    {
        $this->affectedCustomer = $affectedCustomer;

        return $this;
    }

    /**
     * Get affectedCustomer.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getAffectedCustomer()
    {
        return $this->affectedCustomer;
    }

    /**
     * Set affectedAdmin.
     *
     * @param \AppBundle\Entity\User|null $affectedAdmin
     *
     * @return ServiceList
     */
    public function setAffectedAdmin(\AppBundle\Entity\User $affectedAdmin = null)
    {
        $this->affectedAdmin = $affectedAdmin;

        return $this;
    }

    /**
     * Get affectedAdmin.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getAffectedAdmin()
    {
        return $this->affectedAdmin;
    }

    /**
     * Set invoice.
     *
     * @param \AppBundle\Entity\Invoice|null $invoice
     *
     * @return ServiceList
     */
    public function setInvoice(\AppBundle\Entity\Invoice $invoice = null)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get invoice.
     *
     * @return \AppBundle\Entity\Invoice|null
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * Set hT.
     *
     * @param int $hT
     *
     * @return ServiceList
     */
    public function setHT($hT)
    {
        $this->HT = $hT;

        return $this;
    }

    /**
     * Get hT.
     *
     * @return int
     */
    public function getHT()
    {
        return $this->HT;
    }

    /**
     * Add invoice.
     *
     * @param \AppBundle\Entity\Invoice $invoice
     *
     * @return ServiceList
     */
    public function addInvoice(\AppBundle\Entity\Invoice $invoice)
    {
        $this->invoice[] = $invoice;

        return $this;
    }

    /**
     * Remove invoice.
     *
     * @param \AppBundle\Entity\Invoice $invoice
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeInvoice(\AppBundle\Entity\Invoice $invoice)
    {
        return $this->invoice->removeElement($invoice);
    }
}
