<?php

// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;



use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\ExclusionPolicy;



/**
 * @ORM\Entity(repositoryClass="\AppBundle\Repository\UserRepository")
 * @ORM\Table(name="User")
 * @ExclusionPolicy("all")
 */
class User extends BaseUser
{

  public function __construct() {
    parent::__construct();
    // your own logic
  }


  /**
   * @ORM\Id
   * @ORM\Column(type="integer")
   * @ORM\ManyToOne(targetEntity="affectedAgentBarber", cascade={"persist", "remove"})
   * @ORM\ManyToOne(targetEntity="affectedBarberByAdmin", cascade={"persist", "remove"})
   * @ORM\OneToOne(targetEntity="affectedBarberByAdmin", cascade={"persist", "remove"})
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  protected $id;

  /**
   * @ORM\ManyToOne(targetEntity="user")
   */
  protected $affectedAgentBarber;

  /**
   * @ORM\ManyToOne(targetEntity="user")
   */
  protected $affectedBarberByAdmin;

  /**
   * @ORM\OneToMany(targetEntity="ServiceList", mappedBy="affectedCustomer", cascade={"persist", "remove"})
   */
  protected $affectedService;

  /**
   * @ORM\OneToMany(targetEntity="Card", mappedBy="user", cascade={"persist", "remove"})
   */
  protected $card;

  /**
   * @var integer
   * @ORM\Column(name="soldeCredit", type="integer")
   */
  protected $soldeCredit;

  /**
   * @var string
   * @ORM\Column(name="tokenStripeUser", type="string", length=255, unique=true, nullable=true)
   */
  protected $TokenStripeUser;

  /**
   * @ORM\OneToMany(targetEntity="Appointement",  mappedBy="customer", cascade={"persist", "remove"})
   */
  protected $appointement;

  /**
   * [credited description]
   * @return [type] [description]
   */
  public function credited() {

  }
  /**
  * @param $role
  * @return bool
  */
  public function hasGroup($role) {
    return in_array( (array) $role, (array) $this->getGroups());
  }

  /**
  * @param $role
  * @return bool
  */
  public function hasRole($role) {
    return in_array($role, $this->getRoles());
  }

  /**
  * @return array
  */
  public function getRoles()
  {
    $roles = $this->roles;
    if (null !== $this->getGroups()) {
      $roles = array_merge( (array) $roles);
    }
    return $roles;
  }

  /**
  * Add serviceList.
  *
  * @param \AppBundle\Entity\ServiceList $serviceList
  *
  * @return User
  */
  public function addServiceList(\AppBundle\Entity\ServiceList $serviceList)
  {
    $this->serviceList[] = $serviceList;

    return $this;
  }

  /**
  * Remove serviceList.
  *
  * @param \AppBundle\Entity\ServiceList $serviceList
  *
  * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
  */
  public function removeServiceList(\AppBundle\Entity\ServiceList $serviceList)
  {
    return $this->serviceList->removeElement($serviceList);
  }

  /**
  * Get serviceList.
  *
  * @return \Doctrine\Common\Collections\Collection
  */
  public function getServiceList()
  {
    return $this->serviceList;
  }


  /**
  * Add affectedService.
  *
  * @param \AppBundle\Entity\ServiceList $affectedService
  *
  * @return User
  */
  public function addAffectedService(\AppBundle\Entity\ServiceList $affectedService)
  {
    $this->affectedService[] = $affectedService;

    return $this;
  }

  /**
  * Remove affectedService.
  *
  * @param \AppBundle\Entity\ServiceList $affectedService
  *
  * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
  */
  public function removeAffectedService(\AppBundle\Entity\ServiceList $affectedService)
  {
    return $this->affectedService->removeElement($affectedService);
  }

  /**
  * Get affectedService.
  *
  * @return \Doctrine\Common\Collections\Collection
  */
  public function getAffectedService()
  {
    return $this->affectedService;
  }


  /**
  * Set affectedAgentBarber.
  *
  * @param \AppBundle\Entity\user|null $affectedAgentBarber
  *
  * @return User
  */
  public function setAffectedAgentBarber(\AppBundle\Entity\user $affectedAgentBarber = null)
  {
    $this->affectedAgentBarber = $affectedAgentBarber;

    return $this;
  }

  /**
  * Get affectedAgentBarber.
  *
  * @return \AppBundle\Entity\user|null
  */
  public function getAffectedAgentBarber()
  {
    return $this->affectedAgentBarber;
  }

  /**
  * Set affectedBarberByAdmin.
  *
  * @param \AppBundle\Entity\user|null $affectedBarberByAdmin
  *
  * @return User
  */
  public function setAffectedBarberByAdmin(\AppBundle\Entity\user $affectedBarberByAdmin = null)
  {
    $this->affectedBarberByAdmin = $affectedBarberByAdmin;

    return $this;
  }

  /**
  * Get affectedBarberByAdmin.
  *
  * @return \AppBundle\Entity\user|null
  */
  public function getAffectedBarberByAdmin()
  {
    return $this->affectedBarberByAdmin;
  }

    /**
     * Set soldeCredit.
     *
     * @param int $soldeCredit
     *
     * @return User
     */
    public function setSoldeCredit($soldeCredit)
    {
        $this->soldeCredit = $soldeCredit;

        return $this;
    }

    /**
     * Get soldeCredit.
     *
     * @return int
     */
    public function getSoldeCredit()
    {
        return $this->soldeCredit;
    }

    /**
     * Set tokenStripeUser.
     *
     * @param string $tokenStripeUser
     *
     * @return User
     */
    public function setTokenStripeUser($tokenStripeUser)
    {
        $this->TokenStripeUser = $tokenStripeUser;

        return $this;
    }

    /**
     * Get tokenStripeUser.
     *
     * @return string
     */
    public function getTokenStripeUser()
    {
        return $this->TokenStripeUser;
    }

    /**
     * Add idCard.
     *
     * @param \AppBundle\Entity\Card $idCard
     *
     * @return User
     */
    public function addIdCard(\AppBundle\Entity\Card $idCard)
    {
        $this->id_card[] = $idCard;

        return $this;
    }

    /**
     * Remove idCard.
     *
     * @param \AppBundle\Entity\Card $idCard
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeIdCard(\AppBundle\Entity\Card $idCard)
    {
        return $this->id_card->removeElement($idCard);
    }

    /**
     * Get idCard.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIdCard()
    {
        return $this->id_card;
    }

    /**
     * Set idCard.
     *
     * @param \AppBundle\Entity\Card|null $idCard
     *
     * @return User
     */
    public function setIdCard(\AppBundle\Entity\Card $idCard = null)
    {
        $this->id_card = $idCard;

        return $this;
    }

    /**
     * Add card.
     *
     * @param \AppBundle\Entity\Card $card
     *
     * @return User
     */
    public function addCard(\AppBundle\Entity\Card $card)
    {
        $this->card[] = $card;

        return $this;
    }

    /**
     * Remove card.
     *
     * @param \AppBundle\Entity\Card $card
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCard(\AppBundle\Entity\Card $card)
    {
        return $this->card->removeElement($card);
    }

    /**
     * Get card.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCard()
    {
        return $this->card;
    }
}
