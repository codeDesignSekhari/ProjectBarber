<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
/**
* UserRepository
*
* This class was generated by the Doctrine ORM. Add your own custom
* repository methods below.
*/
class UserRepository extends EntityRepository
{

  public function getUserSince($date = null)
  {
    // return $date;
    // Comme vous le voyez, le délais est redondant ici, l'idéale serait de le rendre configurable via votre bundle
    if (!$date)
    {
      $date = new \DateTime();
      $date->setTimestamp(strtotime('5 minutes ago'));
    }

    $qb = $this->createQueryBuilder('u')
        ->where("u.lastLogin > :date")
        ->setParameter('date', $date);

    return $qb->getQuery()->getResult();
  }

  /**
   * [findByRolesTeam description]
   * @param  [type] $role [description]
   * @return [type]       [description]
   */
  public function findByRolesTeam($roles)
  {
    $qb = $this->createQueryBuilder('u')
        ->where("u.roles Like :roles")
        ->setParameter('roles', $roles);
    return $qb->getQuery()->getResult();
  }

}
