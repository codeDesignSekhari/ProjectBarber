<?php

/**
 * ManageCredit.php
 * @copyright 2018-2019 Barber 2.0
 * @author  SEKHARI Youssouf <you.sekhari@gmail.com
 */

namespace AppBundle\Utils;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\User;
use AppBundle\Doctrine\DBAL\Types\JsonType;

use Doctrine\Bundle\DoctrineBundle\Registry;

use Psr\Log\LoggerInterface;
use AppBundle\Entity\ServiceCatalog;

/**
 * App Analytics service
 */
class ManagePrestation
{

    protected $doctrine;
    protected $logger;
    /**
     * AppSettings constructor.
     * @param Registry $doctrine
     */
    public function __construct(Registry $doctrine, LoggerInterface $logger)
    {
        $this->doctrine = $doctrine;
        $this->logger = $logger;
    }

    /**
     * Get doctrine
     * @return Registry
     * @ignore
     */
    private function getDoctrines()
    {
        return $this->doctrine;
    }

    public function createPrestation($credit, $price, $title, $currentAdmin, $flush = false)
    {
        $newPrestation = new ServiceCatalog;
        $newPrestation->setCredit($credit)->setPrice($price)
            ->setAffectedAdmin($currentAdmin)
            ->setHaircuts($title);

        $em = $this->getDoctrines()->getManager();
        $em->persist($newPrestation);
    // $em->flush();
        if ($flush === true) $em->flush();
    }
}
