<?php

namespace BackBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ServiceController extends Controller
{

    /**
     * @Route("/admin/add/service", name="back_service")
     * @Security("has_role('ROLE_ADMIN')")
     **/
    public function creditAction(Request $request)
    {
        $currentAdmin = $this->getUser()->getId();
        return $this->render('@Back/Service/Service.html.twig', [
            "currentAdmin" => $currentAdmin
        ]);
    }
}
