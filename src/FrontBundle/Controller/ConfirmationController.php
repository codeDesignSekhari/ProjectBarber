<?php

namespace FrontBundle\Controller;

// use Symfony\Bundle\FrameworkBundle\Routing\AnnotatedRouteControllerLoader\Route;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ConfirmationController extends Controller
{
    /**
     * @Route("/user/confirmation/purshase", name="front_confirmation_purshase")
     * @Security("has_role('ROLE_FRONT_ACCESS' ) ")
     */
    public function purshaseAction(Request $request)
    {
        return $this->render('@Front/CreditPurchase/confirm-purchase.html.twig');
    }

}
