<?php

namespace FrontBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use AppBundle\Entity\Card;
use FOS\UserBundle\Form\Type\ChangePasswordFormType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ProfileController extends Controller
{

  /**
   * @Route("user/profile", name="front_profile")
   * @Security("has_role('ROLE_FRONT_ACCESS') ")
   **/
  public function profileAction(Request $request)
  {
    $cards = $this->getDoctrine()->getRepository(Card::class)->findByUser($this->getUser());
    return $this->render('@Front/Profile/profile.html.twig',[
      'cards' => $cards 
    ]);
  }

}
