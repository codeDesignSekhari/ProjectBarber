<?php

namespace FrontBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testRouteUser()
    {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'jean',
            'PHP_AUTH_PW'   => '123456',
        ]);
        
        $routes = [
            '/user',
            '/user/prestation',
            '/user/profile',
            '/user/appointment',
            '/user/purchase',
        ];
        foreach ($routes as $route) {
            # code...
            $crawler = $client->request('GET', $route);
            $res = $client->getResponse();
            $this->assertEquals(302, $res->getStatusCode(), 'FAIL: GET '.$route.' return status '. $res->getStatusCode());
        }
    }
}
