function displayCalendar() {
    // var spinner = Createloader();
    $('#calendar').empty().append(Createloader());
    showCalendars();
    // $.get(Routing.generate("api_apifullcalendar_getappointement"))
    //     .done(function(appointments) {});
    // setTimeout(spinnner.remove(), 5000);
    // $('#calendar').append(Createloader()).hide();
}

function showCalendars() {
    var initialLocaleCode = 'fr';
    var hasAlreadyRun = false;
    $('#calendar').fullCalendar({
        defaultView: 'agendaWeek',
        header: {
            left: 'prev,next',
            center: 'title',
            right: 'agendaDay,month,agendaWeek'
        },
        events: {},
        businessHours: {
            // days of week. an array of zero-based day of week integers (0=Sunday)
            dow: [1, 2, 3, 4, 5, 6], // Monday - sunday
            start: '9:00', // a start time (10am in this example)
            end: '21:00', // an end time (6pm in this example)
        },
        slotLabelFormat: [
            'MMMM YYYY', // top level of text
            'ddd' // lower level of text
        ],
        slotDuration: "00:30:00",
        defaultTimedEventDuration: "01:00:00",
        displayEventTime: true,
        locale: initialLocaleCode,
        nowIndicator: true,
        maxTime: "21:00",
        minTime: "9:00",
        selectOverlap: false,
        unselectAuto: true,
        allDaySlot: false,
        timeFormat: 'HH:mm',
        slotLabelFormat: "HH:mm",
        axisFormat: 'HH:mm',
        selectable: true,
        eventSources: [{
            url: Routing.generate('api_apifullcalendar_getappointement'),
            type: 'GET',
            color: "#65a9d7",
            textColor: "#3c3d3d",
            success: function(data) {
                var dataEvent = [];
                for (var key in data) {
                    // console.log(moment(data[key].start_appointement).format());
                    $('#calendar').fullCalendar('renderEvents', [{ start: moment(data[key].start_appointement).format() }], 'stick');
                };
                // $('#calendar').fullCalendar('refetchEventSources', data);
                $('.preloader-wrapper').hide();
                $('.textLoad').hide();
                // $('#calendar').fullCalendar('refetchEvents');
            },
            error: function() {
                displayToast('Un probléme est survenue au moment de la récupération des événements', 'error');
            }
        }],
        dayClick: function(date, jsEvent) {
            var startAppointment = moment().format(date.format());
        },
        select: function(startEvent, endEvent, jsEvent, view) {
            var startAppointment = moment(startEvent.format('YYYY/MM/DD HH:mm:ss')).format('YYYY/MM/DD HH:mm:ss');
            var datedisplay = moment().format(startEvent.format('HH:mm'));
            var endAppointment = moment().format(endEvent.format('HH:mm'));
            $('#dateSelected').html(datedisplay);
            $('#dateSelectedEnd').html(endAppointment);

            $('#addEvents').modal('open');
            // var selectedBarber = $("#affectedBarber").data('affectedagentBarber');
            $('#btnAddEvent').on('click', function(e) {
                e.preventDefault();
                var selectedPrestationId = $("#selectedPrestation").find(':selected').data('price');
                var SelectedCredit = $("#selectedPrestation").find(':selected').data('credit');
                var selectedPrestation = $("#selectedPrestation option:selected").text();
                var selectedBarber = $("#selectedBarber").find(':selected').data('agent');
                // console.log('selectedBarber =>', selectedBarber)
                // if (!selectedBarber) {
                //     console.log('test');
                //     selectedBarber = $("#affectedBarber").data('affectedagentBarber');
                // };
                if (selectedPrestation) {
                    $.ajax(Routing.generate('api_apifullcalendar_postappointement'), {
                        data: {
                            start: startAppointment,
                            end: endAppointment,
                            selectedBarber: selectedBarber,
                            idPrestation: selectedPrestationId,
                            prestation: selectedPrestation,
                            SelectedCredit: SelectedCredit
                        },
                        type: "POST",
                    }).done(function(response) {
                        $('#calendar').fullCalendar('unselect');
                        displayToast(response, 'success');
                        // $('#calendar').fullCalendar('renderEvent', data, true);
                    }).fail(function(response) {
                        console.log(response.responseText)
                        displayToast(response, 'error');
                    });
                    $('#calendar').fullCalendar('unselect');
                } else {
                    $('#calendar').fullCalendar('unselect');
                    displayToast("Le champ intitulé de la prestation est vide", 'notice');
                }

            });
            $('#calendar').fullCalendar('unselect');
            $('#titleEvent').val('');
            $('#startEndDate').val('');
            $("#selectedBarber option:selected").val("")
            jQuery(document).on('keyup', function(evt) {
                if (evt.keyCode == 27) {
                    $('#calendar').fullCalendar('unselect');
                    $('#titleEvent').val('');
                    $('#startEndDate').val('');
                    $("#selectedBarber option:selected").val("")
                    $('#dateSelected').html('');
                    $('#calendar').fullCalendar('refetchEvents');
                }
            });
        },

    })
};

$('#btnClose').on('click', function(e) {
    console.log('clique sur annuler');
    e.preventDefault();
    $('#titleEvent').val('');
    $("#selectedBarber option:selected").val("")
    $('#startEndDate').html('');
    $('#calendar').fullCalendar('unselect');
});

function Createloader() {
    let container = $('<div class="preloader-wrapper big active"></div>')
    let spinnerClipper = $('<div class="circle-clipper left"></div>');
    let spinnerClipperRight = $('<div class="circle-clipper right"></div>');
    let load = $('p').text('Chargement..');
    load.addClass("textLoad");
    let spinnerLeft = spinnerClipper.append($('<div class="circle"></div>'));
    let gatPatch = $('<div class="gap-patch"></div>').append($('<div class="circle"></div>'));
    let circleInSpinneRight = spinnerClipperRight.append($('<div class="circle"></div>'));
    let spinnerLoader = $('<div class="spinner-layer spinner-red-only"></div>')
        .append(spinnerLeft, gatPatch, circleInSpinneRight);


    let spinner = container.append(spinnerLoader);
    let spinnerLoaderR = $('#calendar').append(spinner, load);
    return spinnerLoaderR;
};

$(document).ready(function() {
    $('#addEvents').modal();
    $('#showEvents').modal();
    displayCalendar();
});